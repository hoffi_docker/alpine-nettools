FROM alpine:latest
MAINTAINER Dirk Hoffmann <Dirk.Hoffmann@dell.com>

ENTRYPOINT ["/entrypoint.sh"]

RUN apk --update add bash bash-completion openssh nmap nmap-scripts curl tcpdump bind-tools jq nmap-ncat && \
sed -i s/#PermitRootLogin.*/PermitRootLogin\ yes/ /etc/ssh/sshd_config && rm -rf /var/cache/apk/*


COPY entrypoint.sh /
RUN chmod +x /entrypoint.sh
